// Sriramajayam

#ifndef TR_TENDON_LOAD_DIRECTION_H
#define TR_TENDON_LOAD_DIRECTION_H

#include <tr_LoadDirection.h>

namespace tr
{ 
  //! Class defining a follower load that is oriented towards a fixed point.
  //! Such a direction is realized with tendon loads, with the tendon
  //! routed through a fixed point.
  class TendonLoadDirection: public LoadDirection
    {
    public:
      //! Constructor
      //! \param[in] pnode Node at which load is applied
      //! \param[in] pnum Load number corrersponding to this direction
      //! \param[in] nloads Total number of loads
      //! \param[in] rtcoords Coordinates of the fixed routing point
      TendonLoadDirection(const int nTotalDof, const int pnode,
			const int pnum, const int nloads,
			const double* rtcoords);
   
      //! Destructor
      inline virtual ~TendonLoadDirection() {}
      
      // Disable copy and assignment
      TendonLoadDirection(const TendonLoadDirection&) = delete;
      TendonLoadDirection operator=(const TendonLoadDirection&) = delete;
      
      //! Returns the location of the routing point
      inline const double* GetRoutingPoint() const
      { return rtCoords; }

      //! Returns the loading node
      inline int GetLoadingNode() const
      { return PNode; }
      
      //! Main functionality
      //! Update the load direction and its sensitivity
      virtual void UpdateDirection(const LoadDirectionUpdateParams* obj) override;

    private:
      const int PNode;   //!< Loading node
      const double rtCoords[2]; //!< Routing post information
      std::vector<double> var_Xp; //!< Variations of x-coordinate of loading point
      std::vector<double> var_Yp; //!< Variations of y-coordinate of loading point
    };
}

#endif
