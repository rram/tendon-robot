// Sriramajayam

#ifndef TR_CONSTANT_LOAD_DIRECTION_H
#define TR_CONSTANT_LOAD_DIRECTION_H

#include <tr_LoadDirection.h>
#include <cassert>
#include <cmath>

namespace tr
{
  //! As the name suggests, this class implements
  //! a constant loading direction that does not change
  //! with the solution
  class ConstantLoadDirection: public LoadDirection
  {
  public:
    //! Constructor
    //! \param[in] pnode Node at which load is imposed
    //! \param[in] pnum Load number corresponding to this direction
    //! \param[in] nloads Total number of loads
    //! \param[in] dir Loading direction to use
    inline ConstantLoadDirection(const int nTotalDof, const int pnode,
				 const int pnum, const int nloads,
				 const double* dir)
      :LoadDirection(pnum, nloads),
      PNode(pnode)
    {
      // Check that the provided direction has unit magnitude
      assert(std::abs(dir[0]*dir[0]+dir[1]*dir[1]-1.)<1.e-6 && "tr::ConstantLoadDirection: expected unit vector");

      // Direction cosines
      Hval = dir[0];
      Vval = dir[1];
      
      // Sensitivities: trivial
      dHval.resize(nLoads);
      dVval.resize(nLoads);
      std::fill(dHval.begin(), dHval.end(), 0.);
      std::fill(dVval.begin(), dVval.end(), 0.);
      
       // trivial variations
      dir_deriv_Hval.resize(nTotalDof);
      dir_deriv_Vval.resize(nTotalDof);
      std::fill(dir_deriv_Hval.begin(), dir_deriv_Hval.end(), 0.);
      std::fill(dir_deriv_Vval.begin(), dir_deriv_Vval.end(), 0.);
    }
    
    //! Destructor
    inline virtual ~ConstantLoadDirection() {}
    
    //! Disable copy and assignment
    ConstantLoadDirection(const ConstantLoadDirection&) = delete;
    ConstantLoadDirection operator=(const ConstantLoadDirection&) = delete;
    
    //! Returns the loading node
    inline int GetLoadingNode() const
    { return PNode; }
    
    //! No need to update the direction
    inline virtual void UpdateDirection(const LoadDirectionUpdateParams* params) override
    { return; }

  private:
    const int PNode;
      
  };

}
#endif
