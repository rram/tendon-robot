// Sriramajayam

#include <Element.h>
#include <LocalToGlobalMap.h>
#include <tr_TendonLoadDirection.h>
#include <cmath>
#include <algorithm>

namespace tr
{
  // Constructor
  TendonLoadDirection::TendonLoadDirection(const int nTotalDof, const int pnode,
					   const int pnum, const int nloads,
					   const double* rtcoords)
    :LoadDirection(pnum, nloads),
     PNode(pnode),
     rtCoords{rtcoords[0],rtcoords[1]}
  {
    dHval.resize(nLoads);
    dVval.resize(nLoads);

    dir_deriv_Hval.resize(nTotalDof);
    dir_deriv_Vval.resize(nTotalDof);
    var_Xp.resize(nTotalDof);
    var_Yp.resize(nTotalDof);
     
    // Load directions are state dependent
  } 

  
  void TendonLoadDirection::UpdateDirection(const LoadDirectionUpdateParams* params)
  {
    // Sanity checks
    assert(params!=nullptr && "tr::TendonLoadDirection::UpdateDirection()- parameters not provided");
    assert(static_cast<int>(dHval.size())==nLoads && static_cast<int>(dVval.size())==nLoads);
    const auto& L2GMap = *params->L2GMap;
    const int nTotalDof = L2GMap.GetTotalNumDof();
    assert(static_cast<int>(dir_deriv_Hval.size())==nTotalDof);
    assert(static_cast<int>(dir_deriv_Vval.size())==nTotalDof);
    assert(static_cast<int>(var_Xp.size())==nTotalDof);
    assert(static_cast<int>(var_Yp.size())==nTotalDof);
    
    // Initialize variations   
    std::fill(dir_deriv_Hval.begin(), dir_deriv_Hval.end(), 0.);
    std::fill(dir_deriv_Vval.begin(), dir_deriv_Vval.end(), 0.);
    std::fill(var_Xp.begin(), var_Xp.end(), 0.);
    std::fill(var_Yp.begin(), var_Yp.end(), 0.);
    
    // Access 
    const auto& theta = *(params->theta);
    const auto& ElmArray = *(params->ElmArray);
    const double& Xr = rtCoords[0];
    const double& Yr = rtCoords[1];
    const double& Xp = (params->load_point)[0];
    const double& Yp = (params->load_point)[1];
    const auto& sense_Xp = *(params->load_point_xsensitivity);
    const auto& sense_Yp = *(params->load_point_ysensitivity);
    assert(static_cast<int>(sense_Xp.size())==nLoads && static_cast<int>(sense_Yp.size())==nLoads);

    // Compute new direction cosines: Xp ---> routing point
    const double norm = std::sqrt( (Xr-Xp)*(Xr-Xp) + (Yr-Yp)*(Yr-Yp) );
    assert(norm>1.e-4 && "tr::TendonLoadingDirection::UpdateDirection- tendon length is zero");
    Hval = (Xr-Xp)/norm;
    Vval = (Yr-Yp)/norm;
    
    // Compute sensitivities of new direction cosines
    std::fill(dHval.begin(), dHval.end(), 0.);
    std::fill(dVval.begin(), dVval.end(), 0.);
    for(int L=0; L<nLoads; ++L)
      {
	// Sensitivity of (Xp,Yp) wrt load L
	const double& dXp = sense_Xp[L];
	const double& dYp = sense_Yp[L];
	
	double dnorm = -((Xr-Xp)*dXp + (Yr-Yp)*dYp)/norm;
	dHval[L] = -dXp/norm -(Xr-Xp)*dnorm/(norm*norm);
	dVval[L] = -dYp/norm -(Yr-Yp)*dnorm/(norm*norm);
      }

    // Compute the variation of (Xp, Yp) wrt Na
    for(int e=0; e<PNode-1; ++e)
      {
	const auto& Qwts = ElmArray[e]->GetIntegrationWeights(0);
	const int nQuad = static_cast<int>(Qwts.size());
	const int nDof = ElmArray[e]->GetDof(0);
	for(int q=0; q<nQuad; ++q)
	  {
	    // theta at this quadrature point
	    double thetaval = 0.;
	    for(int a=0; a<nDof; ++a)
	      {
		const int n = L2GMap.Map(0,a,e);
		const double& Na = ElmArray[e]->GetShape(0,q,a);
		thetaval +=  theta[n]*Na;
	      }

	    // Update non-trivial variations over this element
	    for(int a=0; a<nDof; ++a)
	      {
		const int n = L2GMap.Map(0,a,e);
		const double& Na = ElmArray[e]->GetShape(0,q,a);
		var_Xp[n] -= Qwts[q]*std::sin(thetaval)*Na;
		var_Yp[n] += Qwts[q]*std::cos(thetaval)*Na;
	      }
	  }
      }

    // Compute variations of Hval, Vval
    for(int a=0; a<=PNode; ++a)
      {
	const double& v_Xp = var_Xp[a];
	const double& v_Yp = var_Yp[a];
	const double v_norm = -((Xr-Xp)*v_Xp + (Yr-Yp)*v_Yp)/norm;
	dir_deriv_Hval[a] = -v_Xp/norm - (Xr-Xp)*v_norm/(norm*norm);
	dir_deriv_Vval[a] = -v_Yp/norm - (Yr-Yp)*v_norm/(norm*norm);
      }
    
    // done
    return;
  }

}
