// Sriramajayam

#ifndef TR_FOLLOWER_LOAD_DIRECTION_H
#define TR_FOLLOWER_LOAD_DIRECTION_H

#include <tr_LoadDirection.h>
#include <cassert>

namespace tr
{
  //! Class defining a follower load that always
  //! remains normal to the elastica at the loading point
  class FollowerLoadDirection: public LoadDirection
    {
    public:
      //! Constructor
      //! \param[in] pnode Node at which load is applied
      //! \param[in] pnum load number corresponding to this direction
      //! \param[in] nloads Total number of loads
      FollowerLoadDirection(const int nTotalDof, const int pnode, const int pnum, const int nloads);
      
      //! Destructor
      inline virtual ~FollowerLoadDirection() {}

      //! Disable copy and assignment
      FollowerLoadDirection(const FollowerLoadDirection&) = delete;
      FollowerLoadDirection operator=(const FollowerLoadDirection&) = delete;
      
      //! Returns the loading node
      inline int GetLoadingNode() const
      { return PNode; }
      
      //! Main functionality
      //! update the direction cosines and their sensitivities
      virtual void UpdateDirection(const LoadDirectionUpdateParams* obj) override;

    private:
      const int PNode;
    };
}

#endif
