// Sriramajayam

#include <tr_FollowerLoadDirection.h>
#include <LocalToGlobalMap.h>
#include <cmath>
#include <iostream>

namespace tr
{
  // Constructor
  FollowerLoadDirection::FollowerLoadDirection(const int nTotalDof, const int pnode,
					       const int pnum, const int nloads)
    :LoadDirection(pnum, nloads), PNode(pnode)
  {
    dHval.resize(nLoads);
    dVval.resize(nLoads);

    dir_deriv_Hval.resize(nTotalDof);
    dir_deriv_Vval.resize(nTotalDof);

    // Direction cosines, sensitivity and variations are state dependent
  }
  
  // Update the follower load direction and its sensitivities
  void FollowerLoadDirection::UpdateDirection(const LoadDirectionUpdateParams* params)
  {
    // Access parameters
    assert(params!=nullptr && "tr::FollowerLoadDirection::UpdateDirection- parameters not provided");
    const auto& theta = *params->theta;   // State
    const auto& alphaVec = *params->alphaVec; // Sensitivities
    const auto& L2GMap = *params->L2GMap;
    const int nTotalDof = L2GMap.GetTotalNumDof();
    
    // Sanity checks
    assert(static_cast<int>(alphaVec.size())==nLoads);
    assert(static_cast<int>(dHval.size())==nLoads && static_cast<int>(dVval.size())==nLoads);
    assert(static_cast<int>(dir_deriv_Hval.size())==nTotalDof);
    assert(static_cast<int>(dir_deriv_Vval.size())==nTotalDof);

    // Initialize variations
    std::fill(dir_deriv_Hval.begin(), dir_deriv_Hval.end(), 0.);
    std::fill(dir_deriv_Vval.begin(), dir_deriv_Vval.end(), 0.);
    
    // Node number corresponding to this load: PNode
    
    // Get the angle at the load location
    const double thetaval = theta[PNode];

    // Update the load direction for elements (0,...,PNode-1)
    Hval = -std::sin(thetaval);
    Vval = std::cos(thetaval);
    
    // Update the sensitivities of this direction wrt each load
    for(int p=0; p<nLoads; ++p)
      {
	// Get the sensitivity wrt the p-th load 
	const double alphaval = alphaVec[p][PNode];
	
	// Update the sensitivity of the load direction
	dHval[p] = -alphaval*std::cos(thetaval);
	dVval[p] = -alphaval*std::sin(thetaval);
      }

    // Update the directional derivatives of Hval and Vval
    // <dHval, dtheta> and <dHval, dtheta>
    dir_deriv_Hval[PNode] = -std::cos(thetaval);
    dir_deriv_Vval[PNode] = -std::sin(thetaval);

    // done
    return;
  }
  
}
