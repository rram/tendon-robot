// Sriramajayam

#include <tr_LoadedElastica.h>
#include <iostream>
#include <tr_LoadedElastica_State_SNES_Funcs.h>
#include <tr_LoadedElastica_Sensitivity_SNES_Funcs.h>

namespace tr
{
  // Main functionality
  // Computes the solution theta for a given set of loads
  void LoadedElastica::ComputeState(LoadParams& load_params,
				    const std::map<int, double>& dirichlet_bcs,
				    const SolveParams& solve_params)
  {
    // Check parameters
    load_params.Check();
    solve_params.Check();
    const int nDirichlet = static_cast<int>(dirichlet_bcs.size());
    assert(nDirichlet>0 && nDirichlet<=2);
    for(auto& it:dirichlet_bcs)
      { assert(it.first>=0 && it.first<nNodes); }

    if(solve_params.verbose)
      std::cout<<"\ntr::LoadedElastica::Computing state. "<<std::flush;

    // Set Dirichlet bcs into the state.
    // Hence: initial guess automatically satisfies the bcs
    for(auto& it:dirichlet_bcs)
      theta[it.first] = it.second;
    
    // Setup the solver context
    LoadedElastica_StateContext ctx;
    ctx.elastica = this;
    ctx.theta = &theta;
    ctx.Asm = state_Asm;
    ctx.L2GMap = L2GMap;
    ctx.load_params = &load_params;
    ctx.dirichlet_bcs = &dirichlet_bcs;
    ctx.tempVec = &theta_solver.tempVec;
    
    // Solve
    theta_solver.Solve(theta.data(), &ctx, solve_params.verbose);

    // Get the solution
    double* dofValues;
    auto ierr = VecGetArray(theta_solver.solVec, &dofValues); CHKERRV(ierr);
    const int nTotalDof = L2GMap->GetTotalNumDof();
    for(int a=0; a<nTotalDof; ++a)
      theta[a] = dofValues[a];
    ierr = VecRestoreArray(theta_solver.solVec, &dofValues); CHKERRV(ierr);
    
    // Make the state as not dirty, and the sensitivities as dirty
    state_is_dirty = false;
    sense_is_dirty = true;

    // Compute nodal Cartesian coordinates with the new state
    ComputeCartesianCoordinates();

    // done
    return;
  }


  // Computes the sensitivity at a given set of loads
  // The state is recomputed.
  void LoadedElastica::ComputeStateAndSensitivities(LoadParams& load_params,
						    const std::map<int, double>& dirichlet_bcs,
						    const SolveParams& solve_params)
  {
    // Compute the state
    ComputeState(load_params, dirichlet_bcs, solve_params);

    // Setup the solver context
    LoadedElastica_SensitivityContext ctx;
    ctx.LoadNum = -1;
    ctx.elastica = this;
    ctx.theta = &theta;
    ctx.alphaVec = &alphaVec;
    ctx.Asm = sense_Asm;
    ctx.L2GMap = L2GMap;
    ctx.load_params = &load_params;
    ctx.dirichlet_bcs = &dirichlet_bcs;
    ctx.tempVec = &alpha_solver.tempVec;

    // Compute all sensitivities
    for(int L=0; L<nLoads; ++L)
      {
	if(solve_params.verbose) std::cout<<"\ntr::LoadedElastica::Computing sensitivity "<<L<<std::flush;
	ctx.LoadNum = L;
	ctx.Check();

	// Set Dirichlet bcs into the initial guess
	auto& alpha = alphaVec[L];
	for(auto& it:dirichlet_bcs)
	  alpha[it.first] = 0.;

	// Solve
	alpha_solver.Solve(alpha.data(), &ctx, solve_params.verbose);

	// Get the solution
	double* dofValues;
	auto ierr = VecGetArray(alpha_solver.solVec, &dofValues); CHKERRV(ierr);
	const int nTotalDof = L2GMap->GetTotalNumDof();
	for(int a=0; a<nTotalDof; ++a)
	  alpha[a] = dofValues[a];
	ierr = VecRestoreArray(alpha_solver.solVec, &dofValues); CHKERRV(ierr);
      }

    // Note that sensitivities have been computed
    sense_is_dirty = false;
    
    // Compute sensitivities of Cartesian coordinates
    ComputeCartesianCoordinateSensitivities();

    // done
    return;
  }

}
