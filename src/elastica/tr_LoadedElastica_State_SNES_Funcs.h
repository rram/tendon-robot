// Sriramajayam

#ifndef TR_LOADED_ELASTICA_STATE_SNES_FUNCS_H
#define TR_LOADED_ELASTICA_STATE_SNES_FUNCS_H

#include <tr_LoadedElastica.h>

namespace tr
{
  struct LoadedElastica_StateContext
  {
    LoadedElastica* elastica;
    std::vector<double>* theta;
    StandardAssembler<ForcedElasticaOp>* Asm;
    const LocalToGlobalMap* L2GMap;
    LoadParams* load_params;
    const std::map<int, double>* dirichlet_bcs;
    Vec* tempVec;
    inline void Check() const
    {
      assert(elastica!=nullptr);
      assert(theta!=nullptr);
      assert(Asm!=nullptr);
      assert(L2GMap!=nullptr);
      assert(load_params!=nullptr);
      assert(dirichlet_bcs!=nullptr);
      assert(tempVec!=nullptr);
    }
  };

  
  // Assemble residual for the state
  inline PetscErrorCode LoadedElastica_State_Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr)
  {
    // Get the context
    LoadedElastica_StateContext* ctx;
    auto ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr);
    ctx->Check();

    // Unpackage
    auto& elastica = *ctx->elastica;
    auto& theta = *ctx->theta;
    auto& Asm = *ctx->Asm;
    const auto& L2GMap = *ctx->L2GMap;
    auto& load_params = *ctx->load_params;
    const auto& dirichlet_bcs = *ctx->dirichlet_bcs;

    // Solution guess -> theta
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    double* dofArray;
    VecGetArray(solVec, &dofArray);
    for(int n=0; n<nDofs; ++n)
      theta[n] = dofArray[n];
    VecRestoreArray(solVec, &dofArray);

    // Update loading directions to this state
    elastica.UpdateLoadDirections(load_params.dirs);

    // Prepare for assembly
    ForcedElasticaConfiguration config;
    config.L2GMap = &L2GMap;
    config.theta = &theta;
    config.load_params = &load_params;
    config.Check();
    
    // Assemble the residual
    Asm.Assemble(&config, resVec);

    // Set dirichlet BCs into residual
    for(const auto& it:dirichlet_bcs)
      {
	const int& node = it.first;
	const double& target = it.second;
	double value = theta[node]-target;
	ierr = VecSetValues(resVec, 1, &node, &value, INSERT_VALUES); CHKERRQ(ierr);
      }
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);


    // -- done --
    return 0;
  }


  // Function for evaluating the Jacobian
  inline PetscErrorCode LoadedElastica_State_Jacobian_Func(SNES snes, Vec solVec, Mat kMat,
							   Mat pMat, void* usr)
  {
    // Get the context
    LoadedElastica_StateContext* ctx;
    auto ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr && "tr::LoadeElastica_State_Jacobian_Func- context is null");
    ctx->Check();

    // Unpackage
    auto& elastica = *ctx->elastica;
    auto& theta = *ctx->theta;
    auto& Asm = *ctx->Asm;
    const auto& L2GMap = *ctx->L2GMap;
    auto& load_params = *ctx->load_params;
    const auto& dirichlet_bcs = *ctx->dirichlet_bcs;
    auto& tempVec = *ctx->tempVec;

    // Solution guess -> theta
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    double* dofArray;
    VecGetArray(solVec, &dofArray);
    for(int n=0; n<nDofs; ++n)
      theta[n] = dofArray[n];
    VecRestoreArray(solVec, &dofArray);

    // Update loading directions to this state
    elastica.UpdateLoadDirections(load_params.dirs);

    // Prepare for assembly
    ForcedElasticaConfiguration config;
    config.L2GMap = &L2GMap;
    config.theta = &theta;
    config.load_params = &load_params;
    config.Check();

    // Assemble
    Asm.Assemble(&config, tempVec, kMat);

    // Dirichlet BCs
    for(const auto& it:dirichlet_bcs)
      {
	const int& node = it.first;
	ierr = MatZeroRows(kMat, 1, &node, 1., PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);
      }
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

    // -- done --
    return 0;
  }
  
}
  
#endif
