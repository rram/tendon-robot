// Sriramajayam

#include <P11DElement.h>
#include <tr_LoadedElastica.h>
#include <tr_ConstantLoadDirection.h>
#include <tr_FollowerLoadDirection.h>
#include <random>
#include <iostream>

using namespace tr;

int main(int argc, char** argv)
{
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);


  // Create a 1D mesh
  const int nNodes = 3;
  const double h = 1./static_cast<double>(nNodes-1);
  std::vector<double> coordinates(nNodes);
  for(int n=0; n<nNodes; ++n)
    coordinates[n] = 0.+h*static_cast<double>(n);
  const int nElements = nNodes-1;
  std::vector<int> connectivity(2*nElements);
  for(int e=0; e<nElements; ++e)
    { connectivity[2*e] = e+1;
      connectivity[2*e+1] = e+2; }
  Segment<1>::SetGlobalCoordinatesArray(coordinates);

  // Generate random values for the state
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(-2.,2.);
  std::vector<double> theta(nNodes);
  for(int i=0; i<nNodes; ++i)
    theta[i] = dis(gen);
  
  // Dirichlet BCs
  std::map<int,double> dirichlet_bcs{};
  //dirichlet_bcs[0] = 0.;

  // Tolerances for perturbation
  const double pertEPS = 1.e-2;
  const double tolEPS = 1.e-5;

  // 1 end load at a fixed angle
  if(false){
    std::cout<<"\n1 load test, constant direction, end load"<<std::flush;

    const int nLoads = 1;
    std::vector<int> load_nodes{nNodes-1};
    LoadParams LP;
    LP.values.resize(nLoads);
    LP.dirs.resize(nLoads);
    for(int L=0; L<nLoads; ++L)
      {
	LP.values[L] = dis(gen);
	double dir[] = {dis(gen), dis(gen)};
	double norm = std::sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
	dir[0] /= norm;
	dir[1] /= norm;
	LP.dirs[L] = new ConstantLoadDirection(nNodes, load_nodes[L], L, nLoads, dir);
      }
    const double origin[] = {0.,0.};
    LoadedElastica str(coordinates, connectivity, std::sqrt(2.), origin, load_nodes);
    str.StateConsistencyTest(theta.data(), LP, dirichlet_bcs, pertEPS, tolEPS);

    for(int L=0; L<nLoads; ++L)
      delete LP.dirs[L];
  }
  
  // 1 interim follower load
  {
    std::cout<<"\n1 load test, follower load, intermediate point"<<std::flush;
    for(int a=0; a<nNodes; ++a)
      theta[a] = M_PI/3.;
    const int nLoads = 1;
    std::vector<int> load_nodes{nNodes-1};
    LoadParams LP;
    LP.values.resize(nLoads);
    LP.dirs.resize(nLoads);
    for(int L=0; L<nLoads; ++L)
      {
	LP.values[L] = 1.; //dis(gen);
	LP.dirs[L] = new FollowerLoadDirection(nNodes, load_nodes[L], L, nLoads);
      }
    const double origin[] = {0.,0.};
    LoadedElastica str(coordinates, connectivity, std::sqrt(2.), origin, load_nodes);
    str.StateConsistencyTest(theta.data(), LP, dirichlet_bcs, pertEPS, tolEPS);

    for(int L=0; L<nLoads; ++L)
      delete LP.dirs[L];
  }
  exit(1);
  
  // 1 follower load, 1 fixed load
  {
    std::cout<<"\n2 load test: 1 fixed, 1 follower"<<std::flush;

    const int nLoads = 2;
    std::vector<int> load_nodes{nNodes/3, nNodes-2};
    LoadParams LP;
    LP.values.resize(nLoads);
    LP.dirs.resize(nLoads);
    for(int L=0; L<nLoads; ++L)
      {
	LP.values[L] = dis(gen);
	if(L==0)
	  { double dir[] = {dis(gen), dis(gen)};
	    double norm = std::sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
	    dir[0] /= norm;
	    dir[1] /= norm;
	    LP.dirs[L] = new ConstantLoadDirection(nNodes, load_nodes[L], L, nLoads, dir);
	  }
	else
	  { LP.dirs[L] = new FollowerLoadDirection(nNodes, load_nodes[L], L, nLoads); }
      }
    const double origin[] = {0.,0.};
    LoadedElastica str(coordinates, connectivity, std::sqrt(2.), origin, load_nodes);
    str.StateConsistencyTest(theta.data(), LP, dirichlet_bcs, pertEPS, tolEPS);

    for(int L=0; L<nLoads; ++L)
      delete LP.dirs[L];
  }

  PetscFinalize();
}
