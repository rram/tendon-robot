// Sriramajayam

#include <tr_LoadedElastica.h>
#include <P11DElement.h>
#include <tr_LoadedElastica_State_SNES_Funcs.h>
#include <tr_LoadedElastica_Sensitivity_SNES_Funcs.h>

namespace tr
{
  // Constructor
  LoadedElastica::LoadedElastica(const std::vector<double>& coords,
				 const std::vector<int>& conn,
				 const double ei,
				 const double* datuum,
				 const std::vector<int>& PNodes)
    :coordinates(coords),
     connectivity(conn),
     EI(ei),
     origin{datuum[0], datuum[1]},
     LoadingNodes(PNodes),
     nNodes(static_cast<int>(coords.size())),
     nElements(static_cast<int>(conn.size()/2)),
     nLoads(PNodes.size()),
     state_is_dirty(true),
     sense_is_dirty(true)
  {
    // Check that PETSc has been initialized
    PetscBool isInitialized;
    auto ierr = PetscInitialized(&isInitialized); CHKERRV(ierr);
    assert(isInitialized==PETSC_TRUE);
    
    // Check that the loading nodes are in sequence
    assert(nElements==nNodes-1 && "tr::LoadedElastica- Unexpected number of nodes/elements");
    assert(nLoads>0 && "tr::LoadedElastica- Expected at least one load");
    assert(PNodes[0]>0 && "tr::LoadedElastica- Cannot impose a load at the clamped end");
    for(int p=0; p<nLoads-1; ++p)
      assert(PNodes[p+1]>PNodes[p] && "tr::LoadedElastica- Expected loads in sequence");

    // Create elements
    ElmArray.resize(nElements);
    for(int e=0; e<nElements; ++e)
      ElmArray[e] = new P11DElement<1>(connectivity[2*e], connectivity[2*e+1]);

    // Local to global map
    L2GMap = new StandardP11DMap(ElmArray);

    // State operations
    state_OpsArray.resize(nElements);

    // Sensitivity operations
    sense_OpsArray.resize(nElements);
    
    // Active loads for each element based on this image
    //          P0         P1                 Pn
    // |---------|----------|-----------...---|------
    // or Pn acts at the end
    std::vector<bool> activeLoads(nLoads);
    for(int e=0; e<nElements; ++e)
      {
	// Identify loads which are active over this element
	std::fill(activeLoads.begin(), activeLoads.end(), false);
	const int right_node = connectivity[2*e+1]-1;
	for(int L=0; L<nLoads; ++L)
	  if(right_node<=PNodes[L])
	    activeLoads[L] = true;

	// Create state operation 
	state_OpsArray[e] = new ForcedElasticaOp(e, ElmArray[e], EI, activeLoads);

	// Create sensitivity operation
	sense_OpsArray[e] = new ForcedElasticaSensitivityOp(e, ElmArray[e], EI, activeLoads);
      }
    
    // Assemblers
    state_Asm = new StandardAssembler<ForcedElasticaOp>(state_OpsArray, *L2GMap);
    sense_Asm = new StandardAssembler<ForcedElasticaSensitivityOp>(sense_OpsArray, *L2GMap);

    // State
    theta.resize(nNodes);
    std::fill(theta.begin(), theta.end(), 0.);
    xy.resize(2*nNodes);
    for(int a=0; a<nNodes; ++a)
      { xy[2*a] = coordinates[a];
	xy[2*a+1] = 0.; }


    // Sensitivities
    alphaVec.resize(nLoads);
    x_sensitivity.resize(nLoads);
    y_sensitivity.resize(nLoads);
    for(int L=0; L<nLoads; ++L)
      {
	alphaVec[L].resize(nNodes);
       	std::fill(alphaVec[L].begin(), alphaVec[L].end(), 0.);

	x_sensitivity[L].resize(nNodes);
	std::fill(x_sensitivity[L].begin(), x_sensitivity[L].end(), 0.);

	y_sensitivity[L].resize(nNodes);
	std::fill(y_sensitivity[L].begin(), y_sensitivity[L].end(), 0.);
      }
    
    // State sovler
    std::vector<int> nnz;
    state_Asm->CountNonzeros(nnz);
    theta_solver.Initialize(nnz, LoadedElastica_State_Residual_Func, LoadedElastica_State_Jacobian_Func);
    
    // Sensitivity solver
    nnz.clear();
    sense_Asm->CountNonzeros(nnz);
    alpha_solver.Initialize(nnz, LoadedElastica_Sensitivity_Residual_Func, LoadedElastica_Sensitivity_Jacobian_Func);
    
    // -- done --
  }


  // Destructor
  LoadedElastica::~LoadedElastica()
  {
     for(auto& x:ElmArray) delete x;
      delete L2GMap;
      for(auto& x:state_OpsArray) delete x;
      delete state_Asm;
      for(auto& x:sense_OpsArray) delete x;
      delete sense_Asm;
      theta_solver.Destroy();
      alpha_solver.Destroy();
  }


  // Utility function that updates loading directions based on the current state and sensitivities
  void LoadedElastica::UpdateLoadDirections(std::vector<LoadDirection*>& load_dirs) const
  {
    assert(static_cast<int>(load_dirs.size())==nLoads);

    // Set up parameters for updating load directions
    LoadDirectionUpdateParams LP;
    LP.theta = &theta;
    LP.alphaVec = &alphaVec;
    LP.ElmArray = &ElmArray;
    LP.L2GMap = L2GMap;
    std::vector<double> load_point_xsense(nLoads);
    std::vector<double> load_point_ysense(nLoads);
    for(int L=0; L<nLoads; ++L)
      {
	// complete setting up parameters
	LP.load_point = &xy[2*LoadingNodes[L]];
	for(int k=0; k<nLoads; ++k)
	  {
	    load_point_xsense[k] = x_sensitivity[k][LoadingNodes[L]];
	    load_point_ysense[k] = y_sensitivity[k][LoadingNodes[L]];
	  }
	LP.load_point_xsensitivity = &load_point_xsense;
	LP.load_point_ysensitivity = &load_point_ysense;
	LP.Check();

	// Update the load direction
	load_dirs[L]->UpdateDirection(&LP);
      }

    // done
    return;
  }

}
