// Sriramajayam

#ifndef TR_LOADED_ELASTICA_H
#define TR_LOADED_ELASTICA_H

#include <array>
#include <Element.h>
#include <LocalToGlobalMap.h>
#include <Assembler.h>
#include <SNES_Solver.h>
#include <tr_Structs.h>
#include <tr_ForcedElasticaOp.h>
#include <tr_ForcedElasticaSensitivityOp.h>
#include <map>

namespace tr
{
  class LoadedElastica
  {
  public:
    //! Constructor
    //! \param[in] coords Nodal coordinates (1D). Referred to, not copied
    //! \param[in] conn Element connectuivities. Referred to, not copied
    //! \param[in] ei Modulus. Copied
    //! \param[in] pnodes Nodes at which the loads are imposed. Used to partition the element set
    //! Assumed to be in increasing order, numbered from 0. Referred to, not copied
    //! Left end is assumed to be clamped
    //! \warning Assumes that the global coordinates have been set
    LoadedElastica(const std::vector<double>& coords,  const std::vector<int>& conn,
		   const double ei, const double* datuum, const std::vector<int>& pnodes);

    //! Destructor
    virtual ~LoadedElastica();

    //! Disable copy and assignment
    LoadedElastica(const LoadedElastica&) = delete;
    LoadedElastica& operator=(const LoadedElastica&) = delete;

    //! Returns the element array
    inline const std::vector<Element*>& GetElementArray() const
    { return ElmArray; }

    //! Returns the local to global map
    inline const LocalToGlobalMap& GetLocalToGlobalMap() const
    { return *L2GMap; }

    //! Returns the number of loads
    inline int GetNumLoads() const
    { return nLoads; }

    //! Returns the coordinates array
    inline const std::vector<double>& GetCoordinates() const
    { return coordinates; }

    //! Returns the connectivity
    inline const std::vector<int>& GetConnectivity() const
    { return connectivity; }

    //! Returns the number of elements
    inline int GetNumElements() const
    { return nElements; }

    //! Returns the number of nodes
    inline int GetNumNodes() const
    { return nNodes; }

    //! Returns the state
    inline const std::vector<double>& GetStateField() const
    { return theta; }

    //! Returns the Cartesian coordinates of the nodes
    inline const std::vector<double>& GetCartesianCoordinates() const
    { return xy;}
    
    //! Set the state field
    inline void SetStateField(const double* vals)
    {
      const int nTotalDof = L2GMap->GetTotalNumDof();
      for(int a=0; a<nTotalDof; ++a)
	theta[a] = vals[a];
    }
    
    //! Returns the sensitivity wrt a given a load number
    inline const std::vector<double>& GetSensitivityField(const int loadnum) const
    { return alphaVec[loadnum]; }
    
    //! Returns the sensitivities wrt all loads
    const std::vector<std::vector<double>>& GetSensitivityFields() const
    { return alphaVec; }

    //! Returns the sensitivities of the x-coordinates
    inline const std::vector<double>& GetCoordinateSensitivity(DirCosine direction, const int LoadNum) const
    {
      if(direction==DirCosine::Horizontal)
	return x_sensitivity[LoadNum];
      else
	return y_sensitivity[LoadNum];
    }
    
    //! Main functionality
    //! Computes the solution theta for a given set of loads
    //! \param[in] loads Set of loads
    //! \param[in] solve_params Parameters for the solution algorithm
    virtual void ComputeState(LoadParams& load_params,
			      const std::map<int, double>& dirichlet_bcs,
			      const SolveParams& solve_params);

    //! Computes the sensitivity at a given set of loads
    //! The state is recomputed.
    //! \param[in] loads Set of loads
    //! \param[in] solve_params Parameters for the solution algorithm
    virtual void ComputeStateAndSensitivities(LoadParams& load_params,
					      const std::map<int, double>& dirichlet_bcs,
					      const SolveParams& solve_params);

    
    //! Utility function that updates loading directions based on the current solution
    void UpdateLoadDirections(std::vector<LoadDirection*>& load_dirs) const;

    //! Consistency test for state calculations
    void StateConsistencyTest(const double* stateVals,
			      LoadParams& load_params,
			      const std::map<int,double>& dirichlet_bcs,
			      const double pertEPS, const double tolEPS);

    //! Consistency test for sensitivity calculations
    //void SensitivityConsistencyTest(const int L,
    //				    const double* alphaVals,
    //				    LoadParams& load_params,
    //				    const std::map<int,double>& dirichlet_bcs);

  protected:
    // Compute nodal Cartesian coordinates with the current state
    void ComputeCartesianCoordinates();

    // Compute sensitivities of Cartesian coordinates
    void ComputeCartesianCoordinateSensitivities();
    
    // Members
    const std::vector<double>& coordinates; //!< Reference to nodal coordinates
    const std::vector<int>& connectivity; //!< Reference to element connectivities
    const double EI;
    const double origin[2]; //! Coordinates of node 0
    const std::vector<int> LoadingNodes; //!< Nodes where loads are applied
    const int nNodes; //!< Number of nodes
    const int nElements; //!< Number of elements
    const int nLoads; //!< Number of loads

    std::vector<Element*> ElmArray; //!< Element array
    LocalToGlobalMap* L2GMap; //!< Local to global map

    // For state calculations
    std::vector<ForcedElasticaOp*> state_OpsArray; //!< Operations to compute the state (Laplacian)
    StandardAssembler<ForcedElasticaOp>* state_Asm; //!< Assembler for OpsArray calculations
    std::vector<double> theta; //!< Theta field
    std::vector<double> xy; //!< Nodal Cartesian coordinates
    
    // For sensitivity calculations
    std::vector<ForcedElasticaSensitivityOp*> sense_OpsArray; //!< Operations for sensitivity calcs
    StandardAssembler<ForcedElasticaSensitivityOp>* sense_Asm; //!< Assmbler for sensitivity calcs
    std::vector<std::vector<double>> alphaVec; //!< Sensitivities wrt loads
    std::vector<std::vector<double>> x_sensitivity; //!< Sensitivities of the nodal x-coordinates
    std::vector<std::vector<double>> y_sensitivity; //!< Sensitivities of the nodal y-coordinates

    SNESSolver theta_solver; //!< Nonlinear solver for the state
    SNESSolver alpha_solver; //!< Nonlinear solver for sensitivities

    // Monitor whether state and sensitivities are clean/dirty
    mutable bool state_is_dirty;
    mutable bool sense_is_dirty;
  };
}


#endif
