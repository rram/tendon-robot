// Sriramajayam

#ifndef TR_LOADED_ELASTICA_SENSITIVITY_SNES_FUNCS_H
#define TR_LOADED_ELASTICA_SENSITIVITY_SNES_FUNCS_H

#include <tr_LoadedElastica.h>

namespace tr
{
  struct LoadedElastica_SensitivityContext
  {
    int LoadNum;
    LoadedElastica* elastica;
    const std::vector<double>* theta;
    std::vector<std::vector<double>>* alphaVec;
    StandardAssembler<ForcedElasticaSensitivityOp>* Asm;
    const LocalToGlobalMap* L2GMap;
    LoadParams* load_params;
    const std::map<int, double>* dirichlet_bcs;
    Vec* tempVec;
    inline void Check() const
    {
      assert(elastica!=nullptr);
      assert(theta!=nullptr);
      assert(alphaVec!=nullptr);
      assert(LoadNum>=0 && LoadNum<static_cast<int>(alphaVec->size()));
      assert(Asm!=nullptr);
      assert(L2GMap!=nullptr);
      assert(load_params!=nullptr);
      assert(dirichlet_bcs!=nullptr);
      assert(tempVec!=nullptr);
    }
  };


  // Assemble the residual for the sensitivity
  inline PetscErrorCode
    LoadedElastica_Sensitivity_Residual_Func(SNES snes, Vec solVec, Vec resVec, void* usr)
  {
    // Get the context
    LoadedElastica_SensitivityContext* ctx;
    auto ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr);
    ctx->Check();

    // Unpackage
    auto& elastica = *ctx->elastica;
    const auto& theta = *ctx->theta;
    auto& Asm = *ctx->Asm;
    const int& LoadNum = ctx->LoadNum;
    auto& alphaVec = *ctx->alphaVec;
    auto& alpha = alphaVec[LoadNum];
    const auto& L2GMap = *ctx->L2GMap;
    auto& load_params = *ctx->load_params;
    const auto& dirichlet_bcs = *ctx->dirichlet_bcs;

    // solution guess -> alpha
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    double* dofArray;
    VecGetArray(solVec, &dofArray);
    for(int n=0; n<nDofs; ++n)
      alpha[n] = dofArray[n];
    VecRestoreArray(solVec, &dofArray);

    // Update load directions based on these sensitivities
    elastica.UpdateLoadDirections(load_params.dirs);

    // Prepare for assembly
    ForcedElasticaConfiguration stateConfig;
    stateConfig.L2GMap = &L2GMap;
    stateConfig.theta = &theta;
    stateConfig.load_params = &load_params;
    ForcedElasticaSensitivityConfiguration config;
    config.stateConfig = &stateConfig;
    config.alphaVec = &alphaVec;
    config.LoadNum = LoadNum;
    config.Check();

    // Assemble the residual
    Asm.Assemble(&config, resVec);

    // Set dirichlet BCs into the residual
    for(const auto& it:dirichlet_bcs)
      {
	const int& node = it.first;
	double zero = 0.;
	ierr = VecSetValues(resVec, 1, &node, &zero, INSERT_VALUES); CHKERRQ(ierr);
      }
    ierr = VecAssemblyBegin(resVec); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(resVec); CHKERRQ(ierr);

    // -- done--
    return 0;
  }

  
  // Function for evaluating the Jacobian
  inline PetscErrorCode LoadedElastica_Sensitivity_Jacobian_Func(SNES snes, Vec solVec, Mat kMat,
								 Mat pMat, void* usr)
  {
    // Get the context
    LoadedElastica_SensitivityContext* ctx;
    auto ierr = SNESGetApplicationContext(snes, &ctx); CHKERRQ(ierr);
    assert(ctx!=nullptr);
    ctx->Check();

    // Unpackage
    auto& elastica = *ctx->elastica;
    const auto& theta = *ctx->theta;
    auto& Asm = *ctx->Asm;
    const int& LoadNum = ctx->LoadNum;
    auto& alphaVec = *ctx->alphaVec;
    auto& alpha = alphaVec[LoadNum];
    const auto& L2GMap = *ctx->L2GMap;
    auto& load_params = *ctx->load_params;
    const auto& dirichlet_bcs = *ctx->dirichlet_bcs;
    auto& tempVec = *ctx->tempVec;
    
    // solution guess -> alpha
    int nDofs;
    ierr = VecGetSize(solVec, &nDofs); CHKERRQ(ierr);
    double* dofArray;
    VecGetArray(solVec, &dofArray);
    for(int n=0; n<nDofs; ++n)
      alpha[n] = dofArray[n];
    VecRestoreArray(solVec, &dofArray);

    // Update load directions based on these sensitivities
    elastica.UpdateLoadDirections(load_params.dirs);

    // Prepare for assembly
    ForcedElasticaConfiguration stateConfig;
    stateConfig.L2GMap = &L2GMap;
    stateConfig.theta = &theta;
    stateConfig.load_params = &load_params;
    ForcedElasticaSensitivityConfiguration config;
    config.stateConfig = &stateConfig;
    config.alphaVec = &alphaVec;
    config.LoadNum = LoadNum;
    config.Check();

    // Assemble
    Asm.Assemble(&config, tempVec, kMat);

    // Dirichlet BCs
    for(const auto& it:dirichlet_bcs)
      {
	const int& node = it.first;
	ierr = MatZeroRows(kMat, 1, &node, 1., PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);
      }
    ierr = MatAssemblyBegin(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
    ierr = MatAssemblyEnd(kMat, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

    // -- done --
    return 0;
  }
  
}
#endif
