// Sriramajayam

#include <tr_LoadedElastica.h>
#include <tr_LoadedElastica_State_SNES_Funcs.h>
#include <iostream>

namespace tr
{
  // Consistency test for state calculations
  void LoadedElastica::StateConsistencyTest(const double* stateVals,
					    LoadParams& load_params,
					    const std::map<int,double>& dirichlet_bcs,
					    const double pertEPS, const double tolEPS)
  {
    // Access solver internals
    auto& snes = theta_solver.snes;
    auto& solVec = theta_solver.solVec;
    auto& resVec = theta_solver.resVec;
    auto& kMat = theta_solver.kMat;

    // Compute stiffness matrix at the given state
    // stateVals -> solVec
    const int nDof = L2GMap->GetTotalNumDof();
    PetscErrorCode ierr;
    for(int i=0; i<nDof; ++i)
      { ierr = VecSetValues(solVec, 1, &i, &stateVals[i], INSERT_VALUES); CHKERRV(ierr); }
    ierr = VecAssemblyBegin(solVec); CHKERRV(ierr);
    ierr = VecAssemblyEnd(solVec); CHKERRV(ierr);

    // Dummy set of solve parameters
    SolveParams solve_params{.EPS=1.e-10, .resScale=1., .dofScale=1.,
	.nMaxIters=0, .verbose=true};

    // Context
    LoadedElastica_StateContext ctx;
    ctx.elastica = this;
    ctx.theta = &theta;
    ctx.Asm = state_Asm;
    ctx.L2GMap = L2GMap;
    ctx.load_params = &load_params;
    ctx.dirichlet_bcs = &dirichlet_bcs;
    ctx.tempVec = &theta_solver.tempVec;
    ierr = SNESSetApplicationContext(snes, &ctx); CHKERRV(ierr);

    // Assemble the stiffness
    ierr = LoadedElastica_State_Jacobian_Func(snes, solVec, kMat, kMat, nullptr); CHKERRV(ierr);

    // Compute residuals at perturbed states
    Vec resPlus, resMinus;
    ierr = VecDuplicate(resVec, &resPlus); CHKERRV(ierr);
    ierr = VecDuplicate(resVec, &resMinus); CHKERRV(ierr);

    for(int a=0; a<nDof; ++a)
      if(dirichlet_bcs.find(a)==dirichlet_bcs.end()) // don't alter Dirichlet bcs
	{
	  // Postitive perturbation
	  double pert = pertEPS;
	  ierr = VecSetValues(solVec, 1, &a, &pert, ADD_VALUES); CHKERRV(ierr);
	  ierr = VecAssemblyBegin(solVec); CHKERRV(ierr);
	  ierr = VecAssemblyEnd(solVec); CHKERRV(ierr);
	  ierr = LoadedElastica_State_Residual_Func(snes, solVec, resPlus, nullptr); CHKERRV(ierr);

	  // Negative perturbation
	  pert *= -2.;
	  ierr = VecSetValues(solVec, 1, &a, &pert, ADD_VALUES); CHKERRV(ierr);
	  ierr = VecAssemblyBegin(solVec); CHKERRV(ierr);
	  ierr = VecAssemblyEnd(solVec); CHKERRV(ierr);
	  ierr = LoadedElastica_State_Residual_Func(snes, solVec, resMinus, nullptr); CHKERRV(ierr);

	  // Undo perturbation
	  pert *= -0.5;
	  ierr = VecSetValues(solVec, 1, &a, &pert, ADD_VALUES); CHKERRV(ierr);
	  ierr = VecAssemblyBegin(solVec); CHKERRV(ierr);
	  ierr = VecAssemblyEnd(solVec); CHKERRV(ierr);

	  // Compare
	  for(int b=0; b<nDof; ++b)
	    if(dirichlet_bcs.find(b)==dirichlet_bcs.end())
	      {
		double kval;
		ierr = MatGetValues(kMat, 1, &b, 1, &a, &kval); CHKERRV(ierr);
		double rplus, rminus;
		ierr = VecGetValues(resPlus, 1, &b, &rplus); CHKERRV(ierr);
		ierr = VecGetValues(resMinus, 1, &b, &rminus); CHKERRV(ierr);
		double knum = (rplus-rminus)/(2.*pertEPS);

		std::cout<<"\n"<<kval<<" == "<<knum<<std::flush;
	      }
	}

    // Clean up
    ierr = VecDestroy(&resPlus); CHKERRV(ierr);
    ierr = VecDestroy(&resMinus); CHKERRV(ierr);
  }
	  
    
 
  }
