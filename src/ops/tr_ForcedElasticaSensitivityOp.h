// Sriramajayam

#ifndef TR_FORCED_ELASTICA_SENSITIVITY_OP_H
#define TR_FORCED_ELASTICA_SENSITIVITY_OP_H

#include <tr_ForcedElasticaOp.h>

namespace tr
{
  class ForcedElasticaSensitivityOp: public DResidue
  {
  public:
    //! Constructor
    inline ForcedElasticaSensitivityOp(const int elmnum,
				       const Element* elm,
				       const double ei,
				       const std::vector<bool>& active_loads)
      :DResidue(),
      ElmNum(elmnum),
      Elm(elm),
      Fields({0}),
      EI(ei),
      activeLoads(active_loads) {}

    //! Destructor
    inline virtual ~ForcedElasticaSensitivityOp() {}

    // Disable copy and assignment
    ForcedElasticaSensitivityOp(const ForcedElasticaSensitivityOp&) = delete;
    ForcedElasticaSensitivityOp operator=(const ForcedElasticaSensitivityOp&) = delete;

    // Disable cloning
    inline virtual ForcedElasticaSensitivityOp* Clone() const override
    { return nullptr; }

    //! Returns the element being used
    inline const Element* GetElement() const
    { return Elm; }

    //! Returns the fields used
    inline const std::vector<int>& GetField() const override
    { return Fields; }

    //! Returns the number of dofs for a given field
    inline int GetFieldDof(int fieldnumber) const override
    { return Elm->GetDof(Fields[fieldnumber]); }

    //! Returns the modulus
    inline double GetModulus() const
    { return EI; }

    //! Residual calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    inline void GetVal(const void* Config, std::vector<std::vector<double>>* resval) const override
    { return GetDVal(Config, resval, nullptr); }

    //! Residual and stiffness calculation
    //! \param[in] Config Configuration at which to compute the residual
    //! \param[in] resval Computed local residual
    //! \param[in] dresval Computed local stiffness
    void GetDVal(const void* arg, 
		 std::vector<std::vector<double>>* resval,
		 std::vector<std::vector<std::vector<std::vector<double>>>>* dresval) const override;

    //! Consistency test for this operation
    //! \param[in] Config Configuration at which to perform the consistency test
    //! \param[in] pertEPS Tolerance to use for perturbations
    //! \param[in] tolEPS Tolerance to use for examining outputs
    inline bool ConsistencyTest(const void* arg, const double pertEPS, const double tolEPS) const override
    { return false; }

  private:
    const int ElmNum; //!< Element number
    const Element* Elm; //!< Element for this operation
    std::vector<int> Fields; //!< Field number for this operation
    const double EI; //!< Bending modulus
    const std::vector<bool> activeLoads; //!< Set of active loads over this element
  };
}

#endif
