// Sriramajayam

#include <P11DElement.h>
#include <tr_ForcedElasticaOp.h>
#include <tr_ConstantLoadDirection.h>
#include <tr_FollowerLoadDirection.h>
#include <tr_TendonLoadDirection.h>
#include <random>
#include <iostream>

using namespace tr;

void ConsistencyTest(const std::vector<Element*>& ElmArray,
		     const LocalToGlobalMap& L2GMap,
		     LoadParams& lp,
		     ForcedElasticaOp& Op,
		     std::vector<double>& theta);

int main()
{
  // 1 element, 1 load test
  std::vector<double> coords{std::sqrt(2.), std::sqrt(3.)};
  Segment<1>::SetGlobalCoordinatesArray(coords);
  std::vector<Element*> ElmArray(1);
  ElmArray[0] = new P11DElement<1>(1,2);
  StandardP11DMap L2GMap(ElmArray);
  const int nTotalDof = L2GMap.GetTotalNumDof();
  
  const int nLoads = 1;
  std::vector<bool> activeLoads{true};
  ForcedElasticaOp Op(0, ElmArray[0], std::sqrt(5.), activeLoads);
  
  std::random_device rd;  
  std::mt19937 gen(rd()); 
  std::uniform_real_distribution<> dis(-1., 1.);
  std::vector<double> theta(nTotalDof);
  for(int a=0; a<nTotalDof; ++a)
    theta[a] = dis(gen);
  LoadParams lp;
  lp.values.resize(nLoads);
  lp.values[0] = 2.*dis(gen);
  lp.dirs.resize(nLoads);

  // Constant load
  double dir[] = {dis(gen), dis(gen)};
  double norm = std::sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
  dir[0] /= norm;
  dir[1] /= norm;
  lp.dirs[0] = new ConstantLoadDirection(nTotalDof, 1, 0, 1, dir);
  lp.Check();

  // Perform a consistency test
  ConsistencyTest(ElmArray, L2GMap, lp, Op, theta);

  // Follower load
  delete lp.dirs[0];
  lp.dirs[0] = new FollowerLoadDirection(2, 1, 0, 1);
  lp.Check();

  // Perform a consistency test
  ConsistencyTest(ElmArray, L2GMap, lp, Op, theta);

  // Tendon load
  delete lp.dirs[0];
  const double rtcoords[] = {std::sqrt(2.), std::sqrt(5.)};
  lp.dirs[0] = new TendonLoadDirection(2, 1, 0, 1, rtcoords);
  lp.Check();

  // Perform a consistency test
  ConsistencyTest(ElmArray, L2GMap, lp, Op, theta);
  
  // clean up
  for(auto& e:ElmArray) delete e;
  for(auto& x:lp.dirs) if(x!=nullptr) delete x;
}


void ConsistencyTest(const std::vector<Element*>& ElmArray,
		     const LocalToGlobalMap& L2GMap,
		     LoadParams& lp, ForcedElasticaOp& Op,
		     std::vector<double>& theta)
{
  ForcedElasticaConfiguration config;
  config.L2GMap = &L2GMap;
  config.theta = &theta;
  config.load_params = &lp;
  config.Check();
  
  // Dummies
  const int nLoads = static_cast<int>(lp.values.size());
  double coord[] = {0.,1.};
  std::vector<std::vector<double>> alphaVec(nLoads);
  std::vector<double> xcoord_sense(nLoads), ycoord_sense(nLoads);
  for(int L=0; L<nLoads; ++L)
    alphaVec[L].resize(2);

  // Load direction update parameters
  LoadDirectionUpdateParams updateParams;
  updateParams.theta = &theta;
  updateParams.alphaVec = &alphaVec;
  updateParams.load_point = coord;
  updateParams.load_point_xsensitivity = &xcoord_sense;
  updateParams.load_point_ysensitivity = &ycoord_sense;
  updateParams.ElmArray = &ElmArray;
  updateParams.L2GMap = &L2GMap;
  
  // Consistency test
  std::vector<std::vector<double>> fplus(1, std::vector<double>(2));
  auto fminus = fplus;
  std::vector<std::vector<std::vector<std::vector<double>>>> kmat(1);
  kmat[0].resize(2);
  for(int a=0; a<2; ++a)
    { kmat[0][a].resize(1);
      kmat[0][a][0].resize(2); }

  // Stiffness
  lp.dirs[0]->UpdateDirection(&updateParams);
  Op.GetDVal(&config, nullptr, &kmat);

  const double pertEPS = 1.e-6;
  const double tolEPS = 1.e-5;
  for(int a=0; a<2; ++a)
    {
      // positive perturbation
      theta[a] += pertEPS;
      lp.dirs[0]->UpdateDirection(&updateParams);
      Op.GetVal(&config, &fplus);

      // negative perturbation
      theta[a] -= 2.*pertEPS;
      lp.dirs[0]->UpdateDirection(&updateParams);
      Op.GetVal(&config, &fminus);

      // undo perturbation
      theta[a] += pertEPS;
      lp.dirs[0]->UpdateDirection(&updateParams);

      for(int b=0; b<2; ++b)
	{
	  double knum = (fplus[0][b]-fminus[0][b])/(2.*pertEPS);
	  double kval = kmat[0][b][0][a];
	  assert(std::abs(knum-kval)<tolEPS);
	  //std::cout<<"\n"<<kval<<" == "<<knum<<std::flush;
	}
    }

  return;
}
