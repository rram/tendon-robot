// Sriramajayam

#include <tr_ForcedElasticaOp.h>
#include <LocalToGlobalMap.h>
#include <cmath>

namespace tr
{
  // Residual and stiffness calculation
  void ForcedElasticaOp::GetDVal(const void* arg, 
				 std::vector<std::vector<double>>* funcval,
				 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // Access & checks
    assert(arg!=nullptr && "tr::ForcedElasticaOp::GetDVal- Invalid pointer");
    const auto& params = *static_cast<const ForcedElasticaConfiguration*>(arg);
    params.Check();
    const auto& L2GMap = *params.L2GMap;
    const auto& state = *params.theta;
    const auto& loadParams = *params.load_params;
    assert(activeLoads.size()==loadParams.values.size());
    const int nLoads = static_cast<int>(activeLoads.size());

    // Zero the outputs
    SetZero(funcval, dfuncval);

    // Fields, ndofs, shape function derivatives
    const int field = 0.;
    const int nDof = GetFieldDof(0);
    const int nDeriv = Elm->GetNumDerivatives(field);
    assert(nDeriv==1);

    // Quadrature
    const auto& Qwts = Elm->GetIntegrationWeights(field);
    const int nQuad = static_cast<int>(Qwts.size());

    // Pre-compute force components (constant over the element)
    double Hfrc = 0., Vfrc = 0.;

    // Pre-compute variations of force components (constant over the element)
    std::vector<double> var_Hfrc(nDof), var_Vfrc(nDof);
    std::fill(var_Hfrc.begin(), var_Hfrc.end(), 0.);
    std::fill(var_Vfrc.begin(), var_Vfrc.end(), 0.);

    for(int L=0; L<nLoads; ++L)
      if(activeLoads[L]==true)
	{
	  Hfrc += loadParams.values[L]*loadParams.dirs[L]->GetDirectionCosine(DirCosine::Horizontal);
	  Vfrc += loadParams.values[L]*loadParams.dirs[L]->GetDirectionCosine(DirCosine::Vertical);

	  const auto& var_Hcosine = loadParams.dirs[L]->GetDirectionalDerivatives(DirCosine::Horizontal);
	  const auto& var_Vcosine = loadParams.dirs[L]->GetDirectionalDerivatives(DirCosine::Vertical);
	  for(int a=0; a<nDof; ++a)
	    {
	      const int node = L2GMap.Map(0,a,ElmNum);
	      var_Hfrc[a] += loadParams.values[L]*var_Hcosine[node];
	      var_Vfrc[a] += loadParams.values[L]*var_Vcosine[node];
	    }
	}
		
    // Integrate
    double theta, dtheta;
    for(int q=0; q<nQuad; ++q)
      {
	// theta & theta' here
	theta = 0.;
	dtheta = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    const int node = L2GMap.Map(0,a,ElmNum);
	    theta += state[node]*Elm->GetShape(0,q,a);
	    dtheta += state[node]*Elm->GetDShape(0,q,a,0);
	  }

	// Udpate the force vector
	if(funcval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    {
	      // This variation delta F = N_a
	      const double& Na = Elm->GetShape(0,q,a);
	      const double& dNa = Elm->GetDShape(0,q,a,0);

	      // update
	      (*funcval)[0][a] +=
		Qwts[q]*(EI*dtheta*dNa + Hfrc*std::sin(theta)*Na - Vfrc*std::cos(theta)*Na);
	    }

	// Update the stiffness
	if(dfuncval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    {
	      // This variation delta F = N_a
	      const double& Na = Elm->GetShape(0,q,a);
	      const double& dNa = Elm->GetDShape(0,q,a,0);
	      
	      for(int b=0; b<nDof; ++b)
		{
		  // This variation: delta F = N_b
		  const double& Nb = Elm->GetShape(0,q,b);
		  const double& dNb = Elm->GetDShape(0,q,b,0);

		  (*dfuncval)[0][a][0][b] +=
		    Qwts[q]*(EI*dNa*dNb +
			     Hfrc*std::cos(theta)*Na*Nb + var_Hfrc[b]*std::sin(theta)*Na +
			     Vfrc*std::sin(theta)*Na*Nb - var_Vfrc[b]*std::cos(theta)*Na);
		}
	    }
      }
    return;
  }
  
}
