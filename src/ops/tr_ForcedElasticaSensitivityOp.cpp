// Sriramajayam

#include <tr_ForcedElasticaSensitivityOp.h>
#include <cmath>

namespace tr
{
  // Residual and stiffness calculation
  void ForcedElasticaSensitivityOp::
  GetDVal(const void* arg, 
	  std::vector<std::vector<double>>* funcval,
	  std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
  {
    // Access and checks
    assert(arg!=nullptr && "tr::ForcedElasticaSensitivityOp::GetDVal- Invalid pointer");
    const auto& params = *static_cast<const ForcedElasticaSensitivityConfiguration*>(arg);
    params.Check();
    const auto& stateConfig = *params.stateConfig;
    const auto& L2GMap = *stateConfig.L2GMap;
    const auto& state = *stateConfig.theta;
    const auto& loadParams = *stateConfig.load_params;
    const auto& LoadNum = params.LoadNum;
    const auto& alphaVec = *params.alphaVec;
    const auto& alphaLoad = alphaVec[LoadNum];
    assert(activeLoads.size()==loadParams.values.size());
    const int nLoads = static_cast<int>(activeLoads.size());

    // Zero the outputs
    SetZero(funcval, dfuncval);

    // Details
    const int nDof = GetFieldDof(0);
    const auto& Qwts = Elm->GetIntegrationWeights(0);
    const int nQuad = static_cast<int>(Qwts.size());

    // Pre-compute force components over this element
    double Hfrc = 0., Vfrc = 0.;

    // Pre-compute sensitivities of force components with respect to the given load number over this element
    double dHfrc = 0., dVfrc = 0.;

    // Pre-compute variations of force components <delta (H,V), Na>
    std::vector<double> var_dHfrc(nDof), var_dVfrc(nDof);
    std::fill(var_dHfrc.begin(), var_dHfrc.end(), 0.);
    std::fill(var_dVfrc.begin(), var_dVfrc.end(), 0.);
    
    for(int L=0; L<nLoads; ++L)
      if(activeLoads[L]==true)
	{
	  Hfrc += loadParams.values[L]*loadParams.dirs[L]->GetDirectionCosine(DirCosine::Horizontal);
	  Vfrc += loadParams.values[L]*loadParams.dirs[L]->GetDirectionCosine(DirCosine::Vertical);

	  // d(Hfrc,Vfrc)/dP_loadnum
	  dHfrc += loadParams.values[L]*loadParams.dirs[L]->GetSensitivity(DirCosine::Horizontal)[LoadNum];
	  dVfrc += loadParams.values[L]*loadParams.dirs[L]->GetSensitivity(DirCosine::Vertical)[LoadNum];

	  // var(d(Hfrc, Vfrc)/dP_loadnum)
	  const auto& var_Hcosine = loadParams.dirs[L]->GetDirectionalDerivatives(DirCosine::Horizontal);
	  const auto& var_Vcosine = loadParams.dirs[L]->GetDirectionalDerivatives(DirCosine::Vertical);

	  for(int a=0; a<nDof; ++a)
	    {
	      const int node = L2GMap.Map(0,a,ElmNum);
	      var_dHfrc[a] += loadParams.values[L]*var_Hcosine[node];
	      var_dVfrc[a] += loadParams.values[L]*var_Vcosine[node];
	    }
	}
    if(activeLoads[LoadNum]==true)
      {
	dHfrc += loadParams.dirs[LoadNum]->GetDirectionCosine(DirCosine::Horizontal);
	dVfrc += loadParams.dirs[LoadNum]->GetDirectionCosine(DirCosine::Vertical);
      }

    // Integrate
    double theta;
    double alpha, dalpha;
    for(int q=0; q<nQuad; ++q)
      {
	// theta, alpha, alpha' here
	theta = 0.;
	alpha = 0.;
	dalpha = 0.;
	for(int a=0; a<nDof; ++a)
	  {
	    const int node = L2GMap.Map(0,a,ElmNum);
	    theta += state[node]*Elm->GetShape(0,q,a);
	    alpha += alphaLoad[node]*Elm->GetShape(0,q,a);
	    dalpha += alphaLoad[node]*Elm->GetDShape(0,q,a,0);
	  }

	// Update the residual
	if(funcval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    {
	      // This variation: Na
	      const double& Na = Elm->GetShape(0,q,a);
	      const double& dNa = Elm->GetDShape(0,q,a,0);

	      // update
	      (*funcval)[0][a] += Qwts[q]*
		(EI*dalpha*dNa +
		 dHfrc*std::sin(theta)*Na      - dVfrc*std::cos(theta)*Na +
		 Hfrc*std::cos(theta)*alpha*Na + Vfrc*std::sin(theta)*alpha*Na);
	    }

	// Update the stiffness
	if(dfuncval!=nullptr)
	  for(int a=0; a<nDof; ++a)
	    {
	      // This variation: Na
	      const double& Na = Elm->GetShape(0,q,a);
	      const double& dNa = Elm->GetDShape(0,q,a,0);

	      for(int b=0; b<nDof; ++b)
		{
		  // This variation: Nb
		  const double& Nb = Elm->GetShape(0,q,b);
		  const double& dNb = Elm->GetDShape(0,q,b,0);

		  (*dfuncval)[0][a][0][b] += Qwts[q]*
		    (EI*dNa*dNb +
		     Hfrc*std::cos(theta)*Na*Nb + Vfrc*std::sin(theta)*Na*Nb +
		     var_dHfrc[b]*std::sin(theta)*Na - var_dVfrc[b]*std::cos(theta)*Na);
		}
	    }
      }
    return;
  }
		     
}
