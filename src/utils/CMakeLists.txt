# Sriramajayam

add_library(tr_utils STATIC
  tr_Structs.cpp)

# headers
target_include_directories(tr_utils PUBLIC
  ${DGPP_INCLUDE_DIRS}
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../load-direction>)

# Add required flags
target_compile_features(tr_utils PUBLIC ${tr_COMPILE_FEATURES})

install(FILES
  tr_Structs.h
  DESTINATION ${PROJECT_NAME}/include)
