// Sriramajayam

#include <tr_Structs.h>
#include <cassert>
#include <LocalToGlobalMap.h>
#include <tr_LoadDirection.h>

namespace tr
{
  // Sanity checks on loading parameters
  void LoadParams::Check() const
  {
    const int nLoads = static_cast<int>(values.size());
    assert(static_cast<int>(dirs.size())==nLoads);
    for(int L=0; L<nLoads; ++L)
      { assert(dirs[L]!=nullptr);
	assert(dirs[L]->GetLoadNumber()==L);
	assert(dirs[L]->GetNumLoads()==nLoads); }
    return;
  }

  // Sanity check on parameters
  void SolveParams::Check() const
  { assert(EPS>0. && resScale>0. && dofScale>0. && nMaxIters>0); }
  
  // Sanity checks in 
  void ForcedElasticaConfiguration::Check() const
  {
    assert(L2GMap!=nullptr);
    assert(theta!=nullptr && load_params!=nullptr);
    assert(load_params->values.size()==load_params->dirs.size());
    return;
  }

  // Sanity checks
  void ForcedElasticaSensitivityConfiguration::Check() const
  {
    assert(stateConfig!=nullptr);
    stateConfig->Check();
    const int nLoads = static_cast<int>(stateConfig->load_params->values.size());
    assert(static_cast<int>(alphaVec->size())==nLoads);
    assert(LoadNum>=0 && LoadNum<nLoads);
  }


  // Sanity checks
  void LoadDirectionUpdateParams::Check() const
  {
    assert(theta!=nullptr && alphaVec!=nullptr);
    assert(load_point!=nullptr);
    assert(load_point_xsensitivity!=nullptr && load_point_ysensitivity!=nullptr);
    assert(ElmArray!=nullptr && L2GMap!=nullptr);
    const int nLoads = static_cast<int>(alphaVec->size());
    assert(static_cast<int>(load_point_xsensitivity->size())==nLoads);
    assert(static_cast<int>(load_point_ysensitivity->size())==nLoads);
    const int nTotalDof = L2GMap->GetTotalNumDof();
    assert(static_cast<int>(theta->size())==nTotalDof);
    for(auto& alpha:*alphaVec)
      { assert(static_cast<int>(alpha.size())==nTotalDof); }
  }
    
}
