// Sriramajayam

#ifndef TR_STRUCTS_H
#define TR_STRUCTS_H

#include <vector>

class LocalToGlobalMap;
class Element;

namespace tr
{
  class LoadDirection;
  
  struct LoadParams
  {
    std::vector<double> values; //!< Loading values
    std::vector<LoadDirection*> dirs; //!< Loading directions
    void Check() const;     //!< Sanity checks
  };


  //! Helper struct for algorithmic parameters
  struct SolveParams
  {
    double EPS; //!< Absolute tolerance
    double resScale; //!< Scaling factor for convergence of residuals
    double dofScale; //!< Scaling factor for convergence of dofs
    int nMaxIters; //!< Max number of iterations
    bool verbose; //!< Print convergence details

    //! Sanity check on parameters
    void Check() const;
  };
  

  struct ForcedElasticaConfiguration
  {
    const LocalToGlobalMap* L2GMap;
    const std::vector<double>* theta;
    const LoadParams* load_params;
    void Check() const;
  };

  struct ForcedElasticaSensitivityConfiguration
  {
    const ForcedElasticaConfiguration* stateConfig;
    const std::vector<std::vector<double>>* alphaVec;
    int LoadNum;
    void Check() const;
  };

  
  //! Helper struct for specifying parameters for load direction
  struct LoadDirectionUpdateParams
  {
    const std::vector<double>* theta; //!< Pointer to the state solution
    const std::vector<std::vector<double>>* alphaVec; //!< Pointer to sensitivity solutions
    const double* load_point; //!< Cartesian coordinates of the tendon attachment point
    const std::vector<double>* load_point_xsensitivity; //!< Sensitivities of the x-coordinate, length = nLoads
    const std::vector<double>* load_point_ysensitivity; //!< Sensitivities of the y-coordinate, length = nLoads
    const std::vector<Element*>* ElmArray; //!< Pointer to the element array
    const LocalToGlobalMap* L2GMap; //!< Pointer to the local to global map
    void Check() const; //!< Check that pointers have been set
  };
}

#endif
